import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs/index";
import {DataService} from "../services/data.service";

@Component({
  selector: 'sa-shop-view',
  templateUrl: './shop-view.component.html',
  styleUrls: ['./shop-view.component.scss']
})
export class ShopViewComponent implements OnInit {
  list$: Observable<any>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.list$ = this.dataService.getList();
  }


}
