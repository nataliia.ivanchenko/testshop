import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'sa-goods-list',
  templateUrl: './goods-list.component.html',
  styleUrls: ['./goods-list.component.scss']
})
export class GoodsListComponent implements OnInit {
  @Input('list') list: any[];

  list: any[] = [];
  number = 10;

  constructor() { }

  ngOnInit() {
  }

  loadMore() {
    this.number = this.number + 10;
  }
}
