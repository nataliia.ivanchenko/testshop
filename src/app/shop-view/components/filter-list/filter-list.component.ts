import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'sa-filter-list',
  templateUrl: './filter-list.component.html',
  styleUrls: ['./filter-list.component.scss']
})
export class FilterListComponent implements OnInit {
  filters: FormGroup;

  constructor(fb: FormBuilder) {
    this.filters = fb.group(
      [
        {'minPrice': []},
        {'maxPrice': []},
      ]
    )
  }

  ngOnInit() {
  }
}
