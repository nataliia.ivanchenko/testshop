import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingViewComponent } from './landing-view/landing-view.component';
import { ShopViewComponent } from './shop-view/shop-view.component';
import { BrandListComponent } from './landing-view/components/brand-list/brand-list.component';
import { FilterListComponent } from './shop-view/components/filter-list/filter-list.component';
import { GoodsListComponent } from './shop-view/components/goods-list/goods-list.component';
import { DataService } from './services/data.service';
import { HttpClientModule } from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    LandingViewComponent,
    ShopViewComponent,
    BrandListComponent,
    FilterListComponent,
    GoodsListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
