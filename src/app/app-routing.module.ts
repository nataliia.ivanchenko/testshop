import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LandingViewComponent} from "./landing-view/landing-view.component";
import {ShopViewComponent} from "./shop-view/shop-view.component";

const routes: Routes = [
  {
    path: '',
    component: LandingViewComponent
  },
  {
    path: 'shop',
    component: ShopViewComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
