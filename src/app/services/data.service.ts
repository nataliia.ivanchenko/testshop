import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

export interface Configuration {
  colors: [];
  brands: []
}

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http: HttpClient) { }

  getConfig(){
    let headers = new HttpHeaders();
    return this.http.get(environment.serverUrl + '/options', {headers: headers});
  }

  getList() {
    let headers = new HttpHeaders();
    return this.http.get(environment.serverUrl + '/list', {headers: headers});
  }
}
