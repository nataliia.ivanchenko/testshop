import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs/index";
import { DataService } from "../services/data.service";

@Component({
  selector: 'sa-landing-view',
  templateUrl: './landing-view.component.html',
  styleUrls: ['./landing-view.component.scss']
})
export class LandingViewComponent implements OnInit {
  config$: Observable<any>;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.config$ = this.dataService.getConfig();
  }
}
