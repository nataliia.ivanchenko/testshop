# Hiring event 🎉

## Table of Contents

* [Application](#stock-app)
  * [Requirements](#requirements)
  * [Workflows](#workflows)
    * [Home page](#1-home-page)
    * [Shop page](#2-shop-page)
        * [Filters sidebar](#21-filters-sidebar)
            * [Name filter](#211-name-filter)
            * [Price filter](#212-price-filter)
            * [Color filter](#213-color-filter)
            * [Brand filter](#214-brand-filter)
            * [On Sale filter](#215-on-sale-filter)
            * [Rating filter](#216-rating-filter)
        * [Product Items List](#22-product-items-list)
* [Evaluation](#evaluation)
* [Where to get data from?](#where-to-get-data-from)
* [Browser Support](#browser-support)
* [FAQ](#faq)
* [Useful scripts](#useful-scripts)
* [Hints](#hints)

## Shop app

You gonna build a SPA application (using Angular framework) which will communicate with our API and help user to view product items according to selected filters.

### Requirements

* should use Angular framework (currently it's version 7) with [@angular/cli](https://cli.angular.io/)
* data should be gathered from our custom [`API`](#where-to-get-data-from)
* prefer to use RxJS for requests and async actions
* you are free to use any styling framework
* layouts shouldn't be pixel perfect, please use mockups just for schematic purpose
* default commands from [@angular/cli](https://cli.angular.io/) should not be modified, as them will be used for project evaluation.
* please pay attention that filtering logic on shop page should be implelemnted on client side only
* some of tasks are marked as **Nice to have** - it means that these tasks are not necessary to be implemented but it's nice to have them
* **Nice to have** every async interaction with an API should be indicated by showing user a loading spinner
* **Nice to have** tests are optional (but is plus)

### Workflows

#### 1. Home page

##### Route: `/`

* User should be able to view:
  * website logo on the top left, you can use any logo or [this one](https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Paw_(Animal_Rights_symbol).svg/1024px-Paw_(Animal_Rights_symbol).svg.png)
  * list of 4 brands fetched from [config](#where-to-get-data-from) endpoint
  * link to the shop
* By clicking on the website logo user should stay on the same (home) page
* By clicking on one of the brands user should be redirected to shop with pre-selected brand in filters
* By clicking on 'Go to shop' user should be redirected to shop

Basic mockup:
![home page image](landing-page.png)

#### 2. Shop page

##### Route: `/shop`

* User should be able to view:
  * filters sidebar
  * product items list

Basic mockup:
![company page](shop.png)

#### 2.1 Filters sidebar

* User should be able to view: 
    * logo
    * Reset all filters button
    * name filer
    * price filter
    * color filter - **Nice to have**
    * brand filter
    * on sale filter
    * rating filter - **Nice to have**
* User should be able to reset all filters by clicking on 'Reset all' button
* By clicking on the website logo user should be redirected to home page
* By changing any filter product items should be filtered according to chosen filters

#### 2.1.1 Name filter

* User should be able to type item name in input. The product items should be filtered by this name. 

**Nice to have**
* Input should have a proper placeholder to introduce user to it's ability to autocomplete
* All data should be prefetched from `list` endpoint, refer to [where to get a data?](#where-to-get-data-from)
* User should be able to use only [ISO Basic Latin Alphabet](https://en.wikipedia.org/wiki/ISO_basic_Latin_alphabet) with such variations:
  * lowercase
  * uppercase
  * mixed lowercase and uppercase
* Search component should show suggestions (like a google autocomplete)
* User should be able to choose a suggested name from the suggestions list (via keyboard or mouse) and this name will be populated in input
* After typing (or populating) name in the input filter should be applied to products list with delay in time needed if user will continue to type in the input
* You can use any UI library for autocomplete functionality (like [Material UI](https://material.angular.io/components/autocomplete/overview))

#### 2.1.2 Price filter 

* User should be able to view and edit filter which containes two inputs with enabled validations.
* First input is minimal price of shown product items.
* **Nice to have** First input has predefined value - minimal price of product items. You should find item with lowest price and set as default value for min filter.
* User should be able to type input will following validations:
    * Number only
    * **Nice to have** Can not be less than min product item price
    * Can not be larger than selected max price (second input)
* Second input is maximum price of shown product items
* **Nice to have** Second input has predefined value - highest price of product items. You should find item with highest price and set as default value for max filter.
* User should be able to type input will following validations:
    * Number only
    * **Nice to have** Can not be higher than max market price
    * Can not be lower than selected min price (first input)

#### 2.1.3 Color filter

* It's **Nice to have** task
* User should be able to choose colors from color chooser. Products items will be filtered according to chosen colors
* Selected colors should be marked (e.g. square form of color is changed to round one)
* User should be able to unchoose colors from color chooser clicking on already selected color
* All colors should be prefetched from `config` endpoint, refer to [where to get a data?](#where-to-get-data-from)

#### 2.1.4 Brand filter

* User should be able to choose brands from dropdown. Products items will be filtered according to chosen brand.
* **Nice to have** User should be able to choose brands from multi-select. Products items will be filtered according to chosen brands. 
* You can use any UI library for select (like [Material UI](https://material-ui.com/api/select/))
* All brands should be prefetched from `config` endpoint, refer to [where to get a data?](#where-to-get-data-from)

#### 2.1.5 On Sale filter

* User should be able to check checkbox to show only items on sale (items with `sale: true` should be filtered)

#### 2.1.6 Rating filter

* It's **Nice to have** task
* User should be able to filter items by minimum rating (max number - 5). For example: when user select 4 stars, items with rating 4 or more are shown
* Default state for this filter - no stars selected, items with any ratins are shown
* User should be able to clear selected rating by clicking 'x' button on the right

#### 2.2 Product items list

* User should be able to view list of product items as on mockup (image, name, price in USD, rating)
* **Nice to have** User should be to view "%" icon on the right corner of image marking that item is on sale
* When user opens shop page, list of items should be fetched from [API](https://us-central1-epam-hiring-event.cloudfunctions.net/list)
* User should be able to view 'No items found' text if there are no available product items
* Product items list is re-filtered each time when user change any filter in filters sidebar

Load More button
* User should be able to view 10 product items on start
* After clicking 'Load More' button next 10 items should be added to list of shown product items
* When all items are shown on the page 'Load More' buttons is hidden
* When user change any filter number of shown items should be resetted to default one (10 items)
* Load More logic should be implemented only on client side

### Evaluation

| Story         | Task                | Points | Criteria                                                                                                                    |
|---------------|---------------------|--------|-----------------------------------------------------------------------------------------------------------------------------|
| Landing page  |                     |        |                                                                                                                             |
|               | Logo and Go to shop | 5      | Implement main page with Logo and Go to shop buttons logic                                                                  |
|               | Brands list         | 10     | Show list of brands (5) + Support click with routing to shop page with pre-filtered list by brand (5)                       |
|               |                     |        |                                                                                                                             |
| Shop page     |                     |        |                                                                                                                             |
|               | Products list       | 35     | Show list with filtered product items with Load More functionality                                                          |
|               | Filters sidebar     | 50     | Logo and Reset all (10) + Price filter (15) + Name Filter (10) + Brand Filter (10) + On Sale filter (5)  |
|               |                     |        |                                                                                                                             |
|               | Total:              | 100    |                                                                                                                             |

Notes: 
* each additional task (marked as **Nice to have**) will be evaluated as + 10 points

### Where to get data from?

This is list of endpoints which You have to use:

* GET config `https://us-central1-epam-hiring-event.cloudfunctions.net/options`. Example response: 
```json
{
    "colors": [
        {
            "name": "black",
            "rgb": [0,0,0]
        },
        ...
    ],
    "brands": [
        {
            "name": "TIMBERLAND",
            "logo": "https://epam-hiring-event.firebaseapp.com/assets/timberland.png"
        },
        ...
    ]
}
```

* GET all product items `https://us-central1-epam-hiring-event.cloudfunctions.net/list`. Example response:

```json
[
    {
      "_id": "5c06a0556c8136d931ddb392",
      "index": 0,
      "sale": false,
      "price": 423,
      "rating": 1,
      "brand": "PANAMA JACK",
      "picture": "http://placehold.it/320x180",
      "color": "blue",
      "name": "Winklepicker"
    },
    ...
]
```

### Browser Support

Any browser of your choice. Hope it would be Chrome 😂

### FAQ

#### Can I use state manager like NgRx?

Yes, you can use NgRx. 

#### What if I will prepare unit tests which are failing? Are they still counted in the evaluation?

No, only working unit tests will be counted. Think the unit test coverage is your additional points after the application score.

#### What workflow should I start from?

Check the [evaluations](#evaluation) if you want work based on how we count your score.

#### What if I finished all the workflows when there is still some time left?

You submit the solution and take a rest or add some new feature you consider worth attention 😉

### Useful scripts

Useful scripts from [@angular/cli](https://cli.angular.io/)  could be useful for current task implementation.

To generate an Angular project via a development server:

```js
ng new shop-app
```

To start project

```js
ng serve
```

To run project tests

```js
ng test
```

To run linter

```js
ng lint
```

To generate component

```js
ng generate component [name]
```

To generate service

```js
ng generate service [name]
```

Other useful scripts from [@angular/cli](https://cli.angular.io/) can be found [here](hhttps://angular.io/cli).

### Hints

Optional suggested libraries for the components:

* [Angular Material](https://material.angular.io)
* [ngx-bootstrap](https://valor-software.com/ngx-bootstrap/#typeahead)
* [Kendo UI for Angular](https://www.telerik.com/kendo-angular-ui)
